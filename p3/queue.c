#include <stdio.h>
#include <stdlib.h>
 #include <stdbool.h>
#include "queue.h"


 void queue_ini(Queue q){
 	q.end = 0;
 	q.head = 0;
 	q.letras[0] = '0';
	return;
 }

bool queue_isEmpty (Queue *queue) {
	if (queue->head == queue->end) return true;
	else {
		return false;
	}
}

void queue_insert (Queue *queue, char letra){
	if((queue->end + 1) == queue->head){
		return;
	}

	queue->letras[queue->end] = letra;

	if(queue->end == MAX - 1){
		queue->end = 0;
	} else {
		queue->end++;
	}

	printf("%c", letra);
	return;
}

char queue_extract (Queue *queue) {
	char letra;
	letra = queue->letras[queue->head];
	if(queue->head == MAX - 1){
		queue->head = 0;
	} else {
		queue->head++;
	}
	return letra;
}
