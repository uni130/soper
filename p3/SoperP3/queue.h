#include <stdbool.h>
#ifndef QUEUE_H
#define QUEUE_H
#define MAX 10
typedef struct _Queue {
  	int end;
  	int head;
    char letras[MAX];
 }Queue;
void queue_ini ();
bool queue_isFull (Queue *queue);
bool queue_isEmpty (Queue *queue);
/*Coloca al elemento al final, considerando colas circulares*/
void queue_insert (Queue *queue, char element);
/*Saca el primer elemento de la cola*/
char queue_extract (Queue *queue);

#endif
