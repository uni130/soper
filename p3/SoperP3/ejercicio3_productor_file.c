#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include <errno.h>
#include "queue.h"


int main(){
	Queue* q = NULL;
	char* letra = NULL;
	sem_t* sem_shared_mem = NULL;
	sem_t* sem_empty = NULL;
	letra = (char*) malloc (2*sizeof(char));
	pid_t pid;
	int n,i,error,shared;
	sem_unlink("/sem_shared_mem");
	sem_unlink("/sem_empty");
	if ((sem_shared_mem = sem_open("/sem_shared_mem", O_CREAT, 0600, 1)) == SEM_FAILED) {
		perror("/sem_shared_mem");
		sem_unlink("/sem_shared_mem");
		exit(EXIT_FAILURE);
	}

	if ((sem_empty = sem_open("/sem_empty", O_CREAT, 0600, 1)) == SEM_FAILED) {
		perror("/sem_empty");
		sem_unlink("/sem_empty");
		exit(EXIT_FAILURE);
	}
	shared = open("shared.txt", O_RDWR | O_CREAT,S_IRUSR | S_IWUSR);
	if(shared == -1){
		unlink("shared.txt");
		return EXIT_FAILURE;
	}

	error = ftruncate(shared,sizeof(Queue));
	if(error == -1){
		fprintf(stderr, "Error 1");
		unlink("shared.txt");
		return EXIT_FAILURE;
	}

	q = mmap(NULL, sizeof(*q),PROT_READ | PROT_WRITE, MAP_SHARED, shared, 0);
	if(q == MAP_FAILED){
		perror("Error 2");
		unlink("shared.txt");
		return EXIT_FAILURE;
	}
	queue_ini(q);


	printf("comience a llenar la cola\n");


	if(shared == -1){
		shm_unlink("shared");
		return EXIT_FAILURE;
	}
	while(1){
		sem_wait(sem_empty);
		fgets(letra, sizeof(char)*2, stdin );
		sem_post(sem_empty);
		sem_wait(sem_shared_mem);
		if(feof(stdin)){
			sem_post(sem_shared_mem);
			break;
		}else{
			queue_insert(q,letra[0]);
		}
		sem_post(sem_shared_mem);
	}
	queue_insert(q, '\0');
	if(queue_isEmpty(q))printf("error al llenar la cola\n");
	munmap(q, sizeof(*q));
	return 0;

}
