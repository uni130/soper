#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include "queue.h"

int main(){

	sem_t *sem_shared_mem;
	sem_t *sem_empty;
	if ((sem_shared_mem = sem_open("/sem_shared_mem", O_CREAT, 0600, 1)) == SEM_FAILED) {
		perror("/sem_shared_mem");
		sem_unlink("/sem_shared_mem");
		exit(EXIT_FAILURE);
	}

	if ((sem_empty = sem_open("/sem_empty", O_CREAT, 0600, 1)) == SEM_FAILED) {
		perror("/sem_empty");
		sem_unlink("/sem_empty");
		exit(EXIT_FAILURE);
	}

	sem_unlink("/sem_shared_mem");
	int shared = open("shared.txt",O_RDWR | O_CREAT,S_IRUSR | S_IWUSR);
	if(shared == -1){
		printf("error abriendo el archivo\n");
		return EXIT_FAILURE;
	}
	Queue* queue_productor = mmap(NULL, sizeof(*queue_productor), PROT_READ | PROT_WRITE, MAP_SHARED, shared,0);
	if(queue_productor == MAP_FAILED){
		perror("Error 2");
		shm_unlink("shared.txt");
		return EXIT_FAILURE;
	}

	char char_aux;
		printf("buh\n");
		sem_wait(sem_empty);
		sem_wait(sem_shared_mem);
		printf("bruh\n");
		if(queue_isEmpty(queue_productor) == false){
		char_aux = queue_extract(queue_productor);
		sem_post(sem_shared_mem);
		sem_post(sem_empty);
		while(char_aux != '\0'){
			printf("%c", char_aux);
			sem_wait(sem_shared_mem);
			if(queue_isEmpty(queue_productor) == false){
				char_aux = queue_extract(queue_productor);
				sem_post(sem_shared_mem);
			}else{
				printf("cola vaciada sin exito\n");
				sem_post(sem_shared_mem);
				break;
			}
		}}else{
			printf("la cola estaba vacia\n");
		}
	munmap(queue_productor, sizeof(*queue_productor));
	unlink("shared.txt");
	close(shared);
	return 0;
}
