
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#define SHM_NAME "memoria"
#define NAME_MAX 40

int aleat_num(int inf, int sup);
/**
* @brief manejador_SIGUSR1: marca la primera llegada
* @param sennal para la que se usa sigaction con el objetivo de
* modificar la reaccion del proceso ante ella
*/
void manejador_SIGUSR1(int sig){
	return;
}
typedef struct{
int previous_id; //!< Id of the previous client.
int id; //!< Id of the current client.
char charname[NAME_MAX]; //!< Name of the client.
} ClientInfo;

int main(int argc, char** argv){
    pid_t pid;
    sem_t* sem1 = NULL;
    sem_t* sem2 = NULL;
    sem_t* sem3 = NULL;
    int n,i,error,shared;
    char buff[NAME_MAX];
    ClientInfo* estructura = NULL;
    struct sigaction act;
    act.sa_handler = manejador_SIGUSR1;
    sigemptyset(&(act.sa_mask));
    act.sa_flags = 0;
    if (sigaction(SIGUSR1, &act, NULL) < 0) {
	printf("no he conseguido capturar la señal\n");
	perror("sigaction");
	exit(EXIT_FAILURE);
    }
    srand(time(NULL));
    if(argc < 2){
        return -1;
    }
    if ((sem1 = sem_open("sem1", O_CREAT | O_EXCL, S_IWUSR, 1)) == SEM_FAILED) {
	sem_unlink("sem1");
	perror("sem_open1");
	exit(EXIT_FAILURE);
    }
    if ((sem2 = sem_open("sem2", O_CREAT | O_EXCL, S_IWUSR, 1)) == SEM_FAILED) {
	sem_unlink("sem1");
    	sem_unlink("sem2");
	perror("sem_open2");
	exit(EXIT_FAILURE);
    }
    if ((sem3 = sem_open("sem3", O_CREAT | O_EXCL, S_IWUSR, 1)) == SEM_FAILED) {
	sem_unlink("sem1");
    	sem_unlink("sem2");
    	sem_unlink("sem3");
	perror("sem_open3");
	exit(EXIT_FAILURE);
    }
    shared = shm_open(SHM_NAME, O_RDWR | O_CREAT | O_EXCL | S_IRUSR | S_IWUSR,0);
    if(shared == -1){
        shm_unlink(SHM_NAME);
        return EXIT_FAILURE;
    }
    sem_unlink("sem1");
    sem_unlink("sem2");
    sem_unlink("sem3");
    error = ftruncate(shared,sizeof(ClientInfo*));
    if(error == -1){
        fprintf(stderr, "Error 1");
        shm_unlink(SHM_NAME);
        return EXIT_FAILURE;
    }
    estructura = mmap(NULL, sizeof(estructura),PROT_READ | PROT_WRITE, MAP_SHARED, shared, 0);
    if(estructura == MAP_FAILED){
        fprintf(stderr, "Error 2");
        shm_unlink(SHM_NAME);
        return EXIT_FAILURE;
    }
    estructura->id = 0;
    estructura->previous_id = -1;
    pid = fork();
    n = atoi(argv[1]);
    for(i = 1;i < n;i++){
    	  if (pid < 0) {
    	      perror("fork");
    	      exit(EXIT_FAILURE);
    	  }
    	  if (pid > 0) {
   		pid = fork();
     	  }
    }
    if(pid<0){
        perror("fork");
        exit(EXIT_FAILURE);
    }else if(pid > 0){
        printf("esperando señal\n");
        pause();
	sem_wait(sem1);
	sem_wait(sem2);
	sem_wait(sem3);
        //implementar semaforo
        printf(" nombre usuario:%s\n id actual:%d\n id previo:%d\n",estructura->charname,estructura->id,estructura->previous_id);
	sem_post(sem1);
	sem_post(sem2);
	sem_post(sem3);
        for(i = 0;i < n;i++){
            wait(NULL);
        }
        munmap(estructura, sizeof(*estructura));
        shm_unlink(SHM_NAME);
        close(shared);
	sem_close(sem1);
	sem_close(sem2);
	sem_close(sem3);
    }else{
        //semaforo sin implementar
        sleep(aleat_num(1,10));
	sem_wait(sem1);
        estructura->previous_id++;
	sem_post(sem1);
        fprintf(stdout,"instroduce el nombre:\n");
	sem_wait(sem2);
        fgets(buff,NAME_MAX,stdin);
        memcpy(estructura->charname,buff,strlen(buff)*sizeof(char));
	sem_post(sem2);
	sem_wait(sem3);
        estructura->id++;
	sem_post(sem3);
	printf("un hijo ha llegado\n");
        kill(getppid(),SIGUSR1);
    }
    exit(EXIT_SUCCESS);
}
int aleat_num(int inf, int sup) {
    if (sup < inf) {
        return -1;
    }

    return (int) (inf + (double) rand() / (RAND_MAX + 1.0)*(sup - inf + 1));

}
