/**
* Implementa las funciones usadas para mostrar por pantalla el mapa
* @author Profesores de Sistemas Operativos
*/

#include <ncurses.h>
#define SCREEN_REFRESH 2000 /*Delay de la impresion del mapa por pantalla*/

/**
* Inicia la pantalla
*/
void screen_init() {
	initscr();
	clear();
	noecho();
	cbreak();
	keypad(stdscr, FALSE);
	curs_set(0);
}

/**
* Incluye el caracter en su posicion
* @param row Fila en la que se incluye
* @param col Columna en la que se incluye
* @param symbol Caracter que se va a incluir
*/
void screen_addch(int row, int col, char symbol)
{
	mvaddch(row,col,symbol);
}

/**
* Recarga el mapa en pantalla, con el delay prefijado al comienzo
*/
void screen_refresh()
{
	usleep(SCREEN_REFRESH);
	refresh();
}

/**
* Finaliza la impresion
*/
void screen_end() {
	endwin();
}

