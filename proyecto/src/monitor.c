/**
* Muestra por pantalla el mapa
* @author Profesores de Sistemas Operativos
* @version 2.0 Actualizada por Magdalena Herrera
*/

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <mqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <math.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/sem.h>
#include<sys/types.h>
#include<semaphore.h>
#include <simulador.h>
#include <gamescreen.h>
#include <mapa.h>


/**
* Muestra por pantalla el mapa, caracter a caracter. Tambien refresca la pantalla para que no queden
* restos del mapa anteriormente mostrado
* @param mapa Mapa a mostrar por pantalla
*/
void mapa_print(tipo_mapa *mapa)
{
	int i,j;

	for(j=0;j<MAPA_MAXY;j++) {
		for(i=0;i<MAPA_MAXX;i++) {
			tipo_casilla cas=mapa_get_casilla(mapa,j, i);
			//printf("%c",cas.simbolo);
			screen_addch(j, i, cas.simbolo);
		}
		//printf("\n");
	}
	screen_refresh();
}

/**
* Programa principal
*/
int main() {

	int equipos_vivos = 2;
	tipo_mapa* mapa;
	int shared;
	int j;
	sem_t *sem2;
	sem_t *sem4;

	/*creacion del semaforo de monitor que activa la simulacion*/
	if ((sem2 = sem_open("/sem2", O_CREAT, 0600, 0)) == SEM_FAILED) {
		perror("/sem2");
		sem_unlink("/sem2");
		exit(EXIT_FAILURE);
	}

	/*creacion del semaforo de monitor, activado por el monitor*/
	if ((sem4 = sem_open("/sem4", O_CREAT, 0600, 0)) == SEM_FAILED) {
		perror("/sem4");
		sem_unlink("/sem4");
		exit(EXIT_FAILURE);
	}

	/*Activacion del semaforo*/
	sem_post(sem4);
	/*Espera al semaforo de la simulacion*/
	sem_wait(sem2);
	/*Libera los recursos una vez usados*/
	sem_unlink("/sem2");
	sem_unlink("/sem4");
	printf("Acceso del mapa a memoria compartida\n");
	/*Abre memoria compartida**/
	shared = shm_open(SHM_MAP_NAME, O_RDWR | O_CREAT,S_IRUSR | S_IWUSR);
	if(shared == -1){
		shm_unlink(SHM_MAP_NAME);
		return EXIT_FAILURE;
	}

	/*Mapea la memoria compartida*/
	mapa = mmap(NULL, sizeof(*mapa),PROT_READ | PROT_WRITE, MAP_SHARED, shared, 0);
	if(mapa == MAP_FAILED){
		perror("Error 2");
		shm_unlink(SHM_MAP_NAME);
		return EXIT_FAILURE;
	}
	printf("Inicializando pantalla\n");
	screen_init();

	printf("Inicio bucle de impresion\n");
	while(equipos_vivos >= 2){
		mapa_print(mapa);
		equipos_vivos = 0;
		for(j = 0;j < N_EQUIPOS;j++){
			if(mapa_get_num_naves(mapa, j) > 0){
				equipos_vivos++;
			}
		}
	}
	screen_end();
	shm_unlink(SHM_MAP_NAME);

	exit(EXIT_SUCCESS);
}
