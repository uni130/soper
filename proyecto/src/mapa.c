/**
* Implementacion de las funcines del mapa
* @author Profesores de Sistemas Operativos
* @Version 2.0 actualizada por Miguel Lopez Luque
*/

#include "mapa.h"
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>

/**
* Los simbolos que representan a cada equipo
*/
char symbol_equipos[N_EQUIPOS] ={'A','B','C'};

/**
* Limpia la casilla de modo que deja de estar en el mapa
* @param mapa Puntero al mapa que del que se va a limpiar la casilla
* @param posy Posicion en el eje y de la casilla
* @param posx Posicion en el eje x de la casilla
* @return entero
*/
int mapa_clean_casilla(tipo_mapa *mapa, int posy, int posx)
{
	mapa->casillas[posy][posx].equipo=-1;
	mapa->casillas[posy][posx].numNave=-1;
	mapa->casillas[posy][posx].simbolo=SYMB_VACIO;
	return 0;
}

/**
* Obtiene la casilla en las posiciones indicadas
* @param mapa Puntero al mapa del que se quiere obtener la casilla
* @param posy Posicion de la casilla en el eje y
* @param posx Posicion de la casilla en el eje x
* @return casilla del mapa en la posicion [posx, posy]
*/
tipo_casilla mapa_get_casilla(tipo_mapa *mapa, int posy, int posx)
{
	return mapa->casillas[posy][posx];
}

/**
* Obtiene la distancia de una casilla a otra
* @param mapa Mapa entre cuyas casillas se va a calcular la distancia
* @param oriy Posicion de la casilla de origen en el eje x
* @param orix Posicion de la casilla de origen en el eje y
* @param targety Posicion de la casilla de destino en el eje x
* @param targetx Posicion de la casilla de destino en el eje y
* @param entero que marca la distancia
*/
int mapa_get_distancia(tipo_mapa *mapa, int oriy,int orix,int targety,int targetx)
{
	int distx,disty;

	distx=abs(targetx - orix);
	disty=abs(targety - oriy);
	return (distx > disty)? distx:disty;
}

/**
* Obtiene la nave en la posicion enviada como argumento
* @param mapa Mapa del que se obtiene la nave
* @param equipo Equipo al que pertenece la nave
* @param num_nave Numero de la nave dentro de su equipo
*/
tipo_nave mapa_get_nave(tipo_mapa *mapa, int equipo, int num_nave)
{
	return mapa->info_naves[equipo][num_nave];
}

/**
* Obtiene el numero de naves de un equipo en el mapa
* @param mapa Mapa en el que se cuntan las naves
* @param equipo Equipo del que se van a obtener las naves
* @author Cantidad de naves del equipo
*/
int mapa_get_num_naves(tipo_mapa *mapa, int equipo)
{
	return mapa->num_naves[equipo];
}

/**
* Obtiene el simbolo de la posicion del mapa pasada por argumentos
* @param mapa Mapa del que se obtiene el simbolo
* @param posy Posicion en el eje y del simbolo
* @param posx Posicion en el eje x del simbolo
* @return caracter en la casilla indicada
*/
char mapa_get_symbol(tipo_mapa *mapa, int posy, int posx)
{
	return mapa->casillas[posy][posx].simbolo;
}

/**
* Comprueba si la casilla esta vacia
* @param mapa Mapa del que se comprueba la casilla
* @param posy Posicion en el eje y de la casilla
* @param posx Posicion en el eje x de la casilla
* @return estado de la casilla
*/
bool mapa_is_casilla_vacia(tipo_mapa *mapa, int posy, int posx)
{
	return (mapa->casillas[posy][posx].equipo < 0);
}

/**
* Recarga el mapa 
* @param mapa Mapa a recargar
*/
void mapa_restore(tipo_mapa *mapa)
{
	int i,j;

	for(j=0;j<MAPA_MAXY;j++) {
		for(i=0;i<MAPA_MAXX;i++) {
			tipo_casilla cas = mapa_get_casilla(mapa,j, i);
			if (cas.equipo < 0) {
				mapa_set_symbol(mapa,j, i, SYMB_VACIO);
			}
			else {
				mapa_set_symbol(mapa, j, i,symbol_equipos[cas.equipo]);
			}
		}
	}
}

/**
* Indica el simbolo en las coordenadas pasadas ocmo argumentos
* @param mapa Mapa donde se indican las coordenadas
* @param posy Posicion en el eje y 
* @param posx Posicion en el eje x
* @param symbol Simbolo a introducir
*/
void mapa_set_symbol(tipo_mapa *mapa, int posy, int posx, char symbol)
{
	mapa->casillas[posy][posx].simbolo=symbol;
}

/**
* Coloca la nave en el mapa
* @param mapa Mapa donde se coloca la nave
* @param nave Nave a colocar
* @return entero para la comprobacion de errores de la funcion
*/
int mapa_set_nave(tipo_mapa *mapa, tipo_nave nave)
{
	if (nave.equipo >= N_EQUIPOS) return -1;
	if (nave.numNave >= N_NAVES) return -1;
	mapa->info_naves[nave.equipo][nave.numNave]=nave;
	if (nave.viva) {
		mapa->casillas[nave.posy][nave.posx].equipo=nave.equipo;
		mapa->casillas[nave.posy][nave.posx].numNave=nave.numNave;
		mapa->casillas[nave.posy][nave.posx].simbolo=symbol_equipos[nave.equipo];
	}
	else {
		mapa_clean_casilla(mapa,nave.posy, nave.posx);
	}
	return 0;
}

/**
* Indica el numero de naves de un equipo
* @param mapa Mapa del que se indica el numero de naves
* @param equipo Numero del equipo del que se indican las naves
* @param numNaves Numero de naves del equipo
*/
void mapa_set_num_naves(tipo_mapa *mapa, int equipo, int numNaves)
{
	mapa->num_naves[equipo]=numNaves;
}

/**
* Lanza un misil a las coordenadas destino
* @param mapa Mapa donde se mada el misil
* @param origeny Coordenada origen en el eje y
* @param origenx Coordenada origen en el eje x
* @param targety Coordenada destino en el eje y
* @param targetx Coordenada destino en el eje x
*/
void mapa_send_misil(tipo_mapa *mapa, int origeny, int origenx, int targety, int targetx)
{
	int px=origenx;
	int py=origeny;
	int tx=targetx;
	int ty=targety;
	char ps = mapa_get_symbol(mapa,py, px);
	int nextx, nexty;
	char nexts;

	int run = tx - origenx;
	int rise = ty - origeny;
	float m = ((float) rise) / ((float) run);
	float b = origeny - (m * origenx);
	int inc = (origenx < tx)? 1:-1;

	for (nextx = origenx; (( origenx < tx) && (nextx <= tx)) || (( origenx > tx) && (nextx >= tx)); nextx+=inc)
	{
		// solve for y
		float y = (m * nextx) + b;

		// round to nearest int
		nexty = (y > 0.0) ? floor(y + 0.5) : ceil(y - 0.5);

		if ((nexty < 0) || (nexty >= MAPA_MAXY)) {
			continue;
		}
		nexts = mapa_get_symbol(mapa,nexty, nextx);
		mapa_set_symbol(mapa,nexty,nextx,'*');
		mapa_set_symbol(mapa,py,px,ps);
		usleep(50000);
		px = nextx;
		py= nexty;
		ps = nexts;
	}

	mapa_set_symbol(mapa,py, px,ps);
}

