/**
* @brief Programa principal, se encarga de toda la simulacion, implementa el simulador, los jefes, y las naves
* @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
*/

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <mqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/sem.h>
#include <mapa.h>
#include <sys/types.h>
#include <semaphore.h>
#include <time.h>

/*Caracteres reservados para el tipo de accion en la estructura de los mensajes*/
#define MSG 20

/*Posibles direcciones de movimiento*/
enum Directions{NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST, WEST, NORTH_WEST};

/*Estructura de los mensajes*/
typedef struct {
	int posy;
	int posx;
	int desty;
	int destx;
	int equipo; // Equipo de la nave
	int numNave; // Numero de la nave en el equipo
	char tipo_accion[MSG]; // Accion a realizar
} message;

//Estructura de la cola de mensajes
  struct mq_attr attributes = {
    .mq_flags = 0,
    .mq_maxmsg = 10,
    .mq_curmsgs = 0,
    .mq_msgsize = sizeof(message)
};

/*Variable global para el uso de la alarma*/
int tflag = 1;

/**
* @brief aleat_num: devuelve un numero aleatorio entre los valores introducidos
* @param inf valor mas pequeño que puede devolver
* @param sup valor mas grande que puede devolver
*/
int aleat_num(int inf, int sup) {
    if (sup < inf) {
        return -1;
    }

    return (int) (inf + (double) rand() / (RAND_MAX + 1.0)*(sup - inf + 1));

}

/*Devuelve verdadero si la nave en ese destino se queda sin vida*/
bool ataque_nave(tipo_mapa* mapa,int desty,int destx,int* dequipo,int* dnave){
	bool ret = false;
	if(mapa->casillas[desty][destx].equipo >= 0){
		mapa->info_naves[mapa->casillas[desty][destx].equipo][mapa->casillas[desty][destx].numNave].vida -= ATAQUE_DANO;
		if((mapa->info_naves[mapa->casillas[desty][destx].equipo][mapa->casillas[desty][destx].numNave].vida <= 0)||(mapa->info_naves[mapa->casillas[desty][destx].equipo][mapa->casillas[desty][destx].numNave].viva == false)){
			printf(":target destruido\n");
			(*dequipo) = mapa->casillas[desty][destx].equipo;
			(*dnave) = mapa->casillas[desty][destx].numNave;
			if(mapa->info_naves[mapa->casillas[desty][destx].equipo][mapa->casillas[desty][destx].numNave].viva == true){
				mapa->num_naves[mapa->casillas[desty][destx].equipo]--;
				ret = true;
			}
			mapa->info_naves[mapa->casillas[desty][destx].equipo][mapa->casillas[desty][destx].numNave].viva = false;
			if(mapa_clean_casilla(mapa, desty, destx) == -1){
					printf("ERROR AL LIMPIAR LA CASILLA\n");
			}
			return ret;
		}else{
			printf(":target a %d de vida\n",mapa->info_naves[mapa->casillas[desty][destx].equipo][mapa->casillas[desty][destx].numNave].vida);
		}
	}else{
		printf(": FALLIDO: Casilla target vacia\n");
	}
	return false;
}

/*Manejador que define el comportamiento al recibir SIGTERM*/
void manejador_SIGTERM(int sig){
		int i;
		for(i = 0;i < N_NAVES;i++){
			wait(NULL);
		}
		exit(EXIT_SUCCESS);
		return;
}

/*Manejador que define el comportamiento al recibir SIGINT*/
void manejador_SIGINT(int sig){
		int i;
		for(i = 0;i < N_EQUIPOS;i++){
			wait(NULL);
		}
		mq_unlink("/cola1");
		exit(EXIT_SUCCESS);
		return;
}

/*Manejador que define el comportamiento al recibir SIGALARM*/
void manejador_SIGALARM(int sig){
		tflag = 1;
		return;
}


int main() {

	int shared,error,i,j,aux,pipe_status,numero;
	int k = 1;
	int equipos_vivos = -1;
	int ataquey = -1;
	int ataquex = -1;
	char cadena[40];
	int jefe_id = 0;
	int nave_id = 0;
	int pos;
	bool check = false;
	pid_t pid;
	char readbuffer[40];
	int fd[N_EQUIPOS][2];
	int fdd[N_NAVES][2];
	int direccion;/*Variable para ir guardando la direccion en la que se mueve la nave*/
	int moverx, movery;
	int receive_failed;
	mqd_t queue_msg;
	bool dead;
	int* dnave = NULL;
	int* dequipo = NULL;
	message accion;
	sem_t *sem1;
	sem_t *sem2;
	sem_t *sem4;
	tipo_mapa* mapa = NULL;
	struct sigaction act;
     	act.sa_handler = manejador_SIGTERM;
     	sigemptyset(&(act.sa_mask));
     	act.sa_flags = 0;

			printf("Simulador gestionando MQ\n");
			//Se crea la cola de mensajes
			queue_msg = mq_open("/cola1", O_CREAT | O_RDWR | O_NONBLOCK, S_IRUSR | S_IWUSR, &attributes);
			if(queue_msg == (mqd_t)-1){
				mq_unlink("/cola1");
				close(queue_msg);
				fprintf (stderr, "Error creating the queue\n");
				return EXIT_FAILURE;
			}

			/*Creacion del semaforo de acciones de naves*/
			if ((sem1 = sem_open("/sem1", O_CREAT, 0600, 1)) == SEM_FAILED) {
				perror("/sem1");
				sem_unlink("/sem1");
				exit(EXIT_FAILURE);
			}
			sem_unlink("/sem1");

			/*Creacion del semaforo para el monitor*/
			if ((sem2 = sem_open("/sem2", O_CREAT, 0600, 0)) == SEM_FAILED) {
				perror("/sem2");

				sem_unlink("/sem2");

				exit(EXIT_FAILURE);

			}

			/*Creacion del semaforo del monitor*/
			if ((sem4 = sem_open("/sem4", O_CREAT, 0600, 0)) == SEM_FAILED) {
				perror("/sem4");

				sem_unlink("/sem4");

				exit(EXIT_FAILURE);

			}

			sem_unlink("/sem1");

			/*Reserva memoria compartida**/
			printf("Simulador gestionando SHM\n");
			shared = shm_open(SHM_MAP_NAME, O_RDWR | O_CREAT,S_IRUSR | S_IWUSR);
			if(shared == -1){
				shm_unlink(SHM_MAP_NAME);
				return EXIT_FAILURE;
			}

		error = ftruncate(shared,sizeof(tipo_mapa));
		if(error == -1){
			fprintf(stderr, "Error 1");
			shm_unlink(SHM_MAP_NAME);
			return EXIT_FAILURE;
		}

		mapa = mmap(NULL, sizeof(*mapa),PROT_READ | PROT_WRITE, MAP_SHARED, shared, 0);
		if(mapa == MAP_FAILED){
			perror("Error 2");
			shm_unlink(SHM_MAP_NAME);
			return EXIT_FAILURE;
		}





	/*Colocacion de las naves*/
	printf("Simulador inicializando mapa\n");
	for(i = 0;i < MAPA_MAXX;i++){
		for(j = 0;j < MAPA_MAXY;j++){
			mapa_clean_casilla(mapa, j, i);
		}
	}
	for(i = 0;i < N_EQUIPOS;i++){
		mapa->num_naves[i] = N_NAVES;
		for(j = 0;j < N_NAVES;j++){
			mapa->info_naves[i][j].equipo = i;
			mapa->info_naves[i][j].numNave = j;
			mapa->info_naves[i][j].vida = VIDA_MAX;
			mapa->info_naves[i][j].viva = true;
			pos = (i+(j/(float)N_NAVES))*MAPA_MAXX*MAPA_MAXY/(float)N_EQUIPOS;
			mapa->info_naves[i][j].posy = pos/MAPA_MAXX;
			mapa->info_naves[i][j].posx = pos%MAPA_MAXX;
			if(mapa_set_nave(mapa, mapa->info_naves[i][j]) != 0){
				perror("Error en la colocacion de naves\n");
				exit(EXIT_FAILURE);
			}
		}

	}

	/*Se activa el semaforo del monitor para indicar que el mapa ya esta cargado*/
	sem_post(sem2);
	/*Espera al semaforo activado por el monitor*/
	sem_wait(sem4);
	/*Libera los recursos*/
	sem_unlink("/sem2");
	sem_unlink("/sem4");
	
	/*Creacion de la tuberia*/
	pipe_status = pipe(fd[0]);
	if(pipe_status == -1) {
  		perror("Error creando la tuberia\n");
  		exit(EXIT_FAILURE);
 	}
	pid = fork();

	/*Creacion de los jefes*/
	for(i = 0;i < N_EQUIPOS-1;i++){
		if(pid > 0){
			pipe_status = pipe(fd[i+1]);
			if(pipe_status == -1) {
  				perror("Error creando la tuberia\n");
  				exit(EXIT_FAILURE);
 			}
			jefe_id = i+1;
			pid = fork();
		}if(pid < 0){
			exit(EXIT_FAILURE);
		}
	}

		/*Codigo de la simulacion*/
		if(pid > 0){
		/*Define los manejadores de SIGINT y SIGALARM*/	
		printf("Simulador gestionando Senales\n");
		act.sa_handler = manejador_SIGINT;
		if (sigaction(SIGINT, &act, NULL) < 0) {
           		perror("sigaction");
            		exit(EXIT_FAILURE);
       		}

		act.sa_handler = manejador_SIGALARM;
		if (sigaction(SIGALRM, &act, NULL) < 0) {
           		perror("sigaction");
            		exit(EXIT_FAILURE);
       		}
		i = 0;

		/*Cierra la Escritura de las pipes*/
		for(j = 0;j < N_EQUIPOS;j++){
			close(fd[j][0]);
		}

		dnave = (int*)malloc(sizeof(int));
		dequipo = (int*)malloc(sizeof(int));
		tipo_nave nave;
		
		/*Bucle principal de ejecucion de los turnos*/
		while(1){

			/*Comprueba si se ha activado la alarma*/
			if(tflag == 1){
				printf("\n");
				printf("New Turno\n");
				for(i = 0;i < N_EQUIPOS;i++){
 					write(fd[i][1], "TURNO", strlen("TURNO"));
				}
				printf("Simulador: Nuevo Turno\n");
				tflag = 0;
				alarm(TURNO_SECS);
			}

			/*Lee la cola de mensajes*/
			if(k == 1){
				printf("Simulador: Escuchando cola de mensajes\n");
				k = 0;
			}
			receive_failed = mq_receive(queue_msg, (char*)&accion, sizeof(accion), NULL);
			
			if (receive_failed == -1){
				usleep(100000);
			}else{
				k = 1;
				printf("Simulador: Recibido en cola de mensajes\n");
				if(strcmp(accion.tipo_accion,"MOVER") == 0){
					printf("ACCION MOVER: [%c%d] %d,%d -> %d,%d", accion.equipo + 65, accion.numNave, accion.posy, accion.posx, accion.desty, accion.destx);
					/*Comprobar requisitos*/
					/*Casilla objetivo dentro del mapa*/
					if( accion.destx > MAPA_MAXX || accion.desty > MAPA_MAXY){
						printf("ERROR DESTINO FUERA DE RANGO");
						return -1;
					}


					sem_wait(sem1);

					if(mapa_is_casilla_vacia(mapa, accion.desty, accion.destx)){
						mapa->info_naves[accion.equipo][accion.numNave].posx = accion.destx;
						mapa->info_naves[accion.equipo][accion.numNave].posy = accion.desty;
						if(mapa_set_nave(mapa, mapa_get_nave(mapa,accion.equipo,accion.numNave)) == -1){
							printf("ERROR AL COLOCAR LA NAVE");
							return -1;
						}
						/*Se limpia la casilla de la que se ha ido*/
						if(mapa_clean_casilla(mapa, accion.posy, accion.posx) == -1){
							printf("ERROR AL LIMPIAR LA CASILLA");
							return -1;
						}
					}else{
						printf(":FALLIDO CASILLA LLENA");
					}
					printf("\n");
					sem_post(sem1);
					
				} else if (strcmp(accion.tipo_accion, "ATACAR") == 0){
					printf("ACCION ATACAR: [%c%d] %d,%d -> %d,%d", accion.equipo + 65, accion.numNave, accion.posy, accion.posx, accion.desty, accion.destx);
					/*Comprobar requisitos*/
					/*Casilla objetivo dentro del mapa*/
					if( accion.destx > MAPA_MAXX || accion.desty > MAPA_MAXY){
						printf("ERROR DE DESTINO 1 DE ATAQUE");
						return -1;
					}
					nave = mapa_get_nave(mapa, accion.equipo, accion.numNave);
					/*Comprobar que la distancia hasta la casilla esta dentro del rango de ataque*/
					if(mapa_get_distancia(mapa, nave.posy, nave.posx, accion.desty, accion.destx) > ATAQUE_ALCANCE){
						printf("ERROR DE DESTINO 2 DE ATAQUE");
						return -1;
					}
				
				/*Fork para la concurrencia entre el movimiento de la nave y el misil*/
					pid = fork();
					if(pid == 0){

						sem_wait(sem1);
						dead = ataque_nave(mapa,accion.desty,accion.destx,dequipo,dnave);
						
						sem_post(sem1);
						
						
						if(dead == true){
							strcpy(cadena,"");
							sprintf(cadena,"DESTRUIR %d",(*dnave));
							write(fd[(*dequipo)][1], cadena, strlen(cadena));
						}
						exit(EXIT_SUCCESS);
					}else if(pid == -1){
						printf("ERROR EN EL FORK DE MISIL");
						return -1;
					}else{
						printf("\n");
					}
				} else {
					printf("ERROR DE MENSAJE");
					return -1;
				}
			}
			/*Condiciones para el fin de simulacion*/
			equipos_vivos = 0;
			for(j = 0;j < N_EQUIPOS;j++){
				if(mapa_get_num_naves(mapa, j) > 0){
					equipos_vivos++;
				}
			}
			/*1 o 0 en caso de que las ultimas dos naves se hayan matado
			al mismo tiempo*/
			if(equipos_vivos <= 1){
				for(j = 0;j < N_EQUIPOS;j++){
					write(fd[j][1], "FIN", strlen("FIN"));
				}
				break;
			}
		}

		for(i = 0;i < N_EQUIPOS;i++){
			wait(NULL);
		}

		/*Liberacion de recursos*/
		free(dnave);
		free(dequipo);
		shm_unlink(SHM_MAP_NAME);
		mq_unlink("/cola1");
		close(queue_msg);
	}


	if(pid == 0){
		/*Creacion de las naves*/
		/*Acceso a los recursos que se van a usar, pipes entre la nave y el
		jefe y cola de mensajes entre la nave y el simulador*/
		pipe_status = pipe(fdd[0]);
		if(pipe_status == -1) {
  			perror("Error creando la tuberia\n");
  			exit(EXIT_FAILURE);
 		}

		pid = fork();
		for(i = 0;i < N_NAVES-1;i++){
			if(pid > 0){
				pipe_status = pipe(fdd[i+1]);
				if(pipe_status == -1) {
  					perror("Error creando la tuberia\n");
  					exit(EXIT_FAILURE);
 				}
				nave_id = i+1;
				pid = fork();
			}if(pid < 0){
				exit(EXIT_FAILURE);
			}
		}


		/*Codigo de los jefes*/
		if(pid > 0){
			srand (time(NULL));
			if (sigaction(SIGTERM, &act, NULL) < 0) {
           			perror("sigaction");
            			exit(EXIT_FAILURE);
       			}
			if (sigaction(SIGINT, &act, NULL) < 0) {
           			perror("sigaction");
            			exit(EXIT_FAILURE);
       			}
			close(fd[jefe_id][1]);

			/*Cierra la Lectura del pipe de cada nave que ha creado*/
			for(i = 0;i < N_NAVES;i++){
				close(fdd[i][0]);
			}
			while(1){
				//lectura del pipe con simulador
  				read(fd[jefe_id][0],readbuffer, 40*sizeof(char));
				
				
				/*en el bucle turno el identificador de la nave es la i*/
				if(strncmp(readbuffer,"TURNO",5) == 0){
					
					for(i = 0;i < N_NAVES;i++){
						for(j = 1;j <= 2;j++){
							aux = aleat_num(0,1);
							if(aux == 1){
								strcpy(cadena,"MOVER_ALEATORIO");
							}else{
								strcpy(cadena,"ATACAR");
							}
							write(fdd[i][1],cadena, strlen(cadena));
			

						}
					}


				}else if(strncmp(readbuffer,"FIN",3) == 0){
					kill(0, SIGTERM);
				}else{
					/*Asumimos que es una cadena compuesta*/
					sscanf(readbuffer,"%s %d",cadena,&numero);
					if(strncmp(readbuffer,"DESTRUIR",8) == 0){

						write(fdd[numero][1],cadena, strlen(cadena));
						close(fdd[numero][1]);
					}
				}
			}
		}
	}

	/*Codigo de las naves*/
	if(pid == 0){
		/*Semilla para obtener numeros aleatorios*/
    		srand (time(NULL));

		/*Abrir la cola de mensajes*/
		queue_msg = mq_open("/cola1",O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, &attributes);
  		if(queue_msg == (mqd_t)-1){
    			fprintf (stderr, "Error opening the queue\n");
    			return EXIT_FAILURE;
		}
		/*Cerrar Escritura en el pipe*/
		close(fdd[nave_id][1]);


		while(1){
			/*Lectura del pipe vinculado al Jefe de su equipo*/
			strcpy(readbuffer,"");
  			read(fdd[nave_id][0],readbuffer, 40*sizeof(char));
			
			/*Cuando tiene que atacar porque se lo ordenan por pipes*/
			
			if(strncmp(readbuffer,"ATACAR",6) == 0){
		
				/*Buscar una nave a la que atacar y la forma de hacerlo es empezando por las posiciones mas cercanas y avanzando progresivamente*/
				for(i = 1,j = 0;(i+j) < ATAQUE_ALCANCE;i++){
					for(j = 0;j <= i;j++){
						if((MAPA_MAXY > mapa->info_naves[jefe_id][nave_id].posy + j) && (MAPA_MAXX > mapa->info_naves[jefe_id][nave_id].posx + i)){
							if(mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx + i][mapa->info_naves[jefe_id][nave_id].posy + j].equipo >= 0 && mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx + i][mapa->info_naves[jefe_id][nave_id].posy + j].equipo != jefe_id){
								ataquex = mapa->info_naves[jefe_id][nave_id].posx + i;
								ataquey = mapa->info_naves[jefe_id][nave_id].posy + j;
							}
						}
						if((0 <= mapa->info_naves[jefe_id][nave_id].posy - j) && (MAPA_MAXX > mapa->info_naves[jefe_id][nave_id].posx + i)){
							if(mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx + i][mapa->info_naves[jefe_id][nave_id].posy - j].equipo >= 0 && mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx + i][mapa->info_naves[jefe_id][nave_id].posy - j].equipo != jefe_id){
								ataquex = mapa->info_naves[jefe_id][nave_id].posx + i;
								ataquey = mapa->info_naves[jefe_id][nave_id].posy - j;
							}
						}
						if((MAPA_MAXY > mapa->info_naves[jefe_id][nave_id].posy + j) && (0 <= mapa->info_naves[jefe_id][nave_id].posx - i)){
							if(mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx - i][mapa->info_naves[jefe_id][nave_id].posy + j].equipo >= 0 && mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx - i][mapa->info_naves[jefe_id][nave_id].posy + j].equipo != jefe_id){
								ataquex = mapa->info_naves[jefe_id][nave_id].posx - i;
								ataquey = mapa->info_naves[jefe_id][nave_id].posy + j;
							}
						}
						if((0 <= mapa->info_naves[jefe_id][nave_id].posy - j) && (0 <= mapa->info_naves[jefe_id][nave_id].posx - i)){
							if(mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx - i][mapa->info_naves[jefe_id][nave_id].posy - j].equipo >= 0 && mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx - i][mapa->info_naves[jefe_id][nave_id].posy - j].equipo != jefe_id){
								ataquex = mapa->info_naves[jefe_id][nave_id].posx - i;
								ataquey = mapa->info_naves[jefe_id][nave_id].posy - j;
							}
						}

						/*de no poner esta condicion mirariamos varias veces en las mismas casillas*/
						if(j!=i){
							aux = j;
							j = i;
							i = aux;

							/*a partir de aqui mismas condiciones que antes del if*/
							if((MAPA_MAXY > mapa->info_naves[jefe_id][nave_id].posy + j) && (MAPA_MAXX > mapa->info_naves[jefe_id][nave_id].posx + i)){
								if(mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx + i][mapa->info_naves[jefe_id][nave_id].posy + j].equipo >= 0 && mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx + i][mapa->info_naves[jefe_id][nave_id].posy + j].equipo != jefe_id){
									ataquex = mapa->info_naves[jefe_id][nave_id].posx + i;
									ataquey = mapa->info_naves[jefe_id][nave_id].posy + j;
								}
							}
							if((0 <= mapa->info_naves[jefe_id][nave_id].posy - j) && (MAPA_MAXX > mapa->info_naves[jefe_id][nave_id].posx + i)){
								if(mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx + i][mapa->info_naves[jefe_id][nave_id].posy - j].equipo >= 0 && mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx + i][mapa->info_naves[jefe_id][nave_id].posy - j].equipo != jefe_id){
									ataquex = mapa->info_naves[jefe_id][nave_id].posx + i;
									ataquey = mapa->info_naves[jefe_id][nave_id].posy - j;
								}
							}
							if((MAPA_MAXY > mapa->info_naves[jefe_id][nave_id].posy + j) && (0 <= mapa->info_naves[jefe_id][nave_id].posx - i)){
								if(mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx - i][mapa->info_naves[jefe_id][nave_id].posy + j].equipo >= 0 && mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx - i][mapa->info_naves[jefe_id][nave_id].posy + j].equipo != jefe_id){
									ataquex = mapa->info_naves[jefe_id][nave_id].posx - i;
									ataquey = mapa->info_naves[jefe_id][nave_id].posy + j;
								}
							}
							if((0 <= mapa->info_naves[jefe_id][nave_id].posy - j) && (0 <= mapa->info_naves[jefe_id][nave_id].posx - i)){
								if(mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx - i][mapa->info_naves[jefe_id][nave_id].posy - j].equipo >= 0 && mapa->casillas[mapa->info_naves[jefe_id][nave_id].posx - i][mapa->info_naves[jefe_id][nave_id].posy - j].equipo != jefe_id){
									ataquex = mapa->info_naves[jefe_id][nave_id].posx - i;
									ataquey = mapa->info_naves[jefe_id][nave_id].posy - j;
								}
							}
							aux = j;
							j = i;
							i = aux;
						}




						if(ataquex >= 0){
							break;
						}
					}
					if(ataquex >= 0){
						break;
					}
				}
				
				
				if(ataquey != -1){
					
					
					/*Se escribe el mensaje*/
					message msg;
					msg.posy = mapa_get_nave(mapa,jefe_id,nave_id).posy;
					msg.posx = mapa_get_nave(mapa,jefe_id,nave_id).posx;
					msg.desty = ataquex;
					msg.destx = ataquey;
					msg.numNave = nave_id;
					msg.equipo = jefe_id;
					strcpy(msg.tipo_accion,"ATACAR");
					
					/*Se manda el mensaje*/
					if(mq_send(queue_msg, (char *)&msg, sizeof(msg), 1) == -1){
						fprintf (stderr, "Error sending message\n");
						return EXIT_FAILURE;
					}
					ataquex = -1;
					ataquey = -1;
				}
			}else if(strncmp(readbuffer,"MOVER_ALEATORIO",15) == 0){
			
			/*Cuando tiene que moverse porque se lo ordenan por pipes*/

				/*Buscar una casilla a la que moverse*/
				check = false;

				/*longitud = rand() % MOVER_ALCANCE*/
				direccion = aleat_num(0,7);
				i = 0;
				while(check == false){
					direccion = (direccion + 1) % 8;
					i++;
					switch(direccion){
						case NORTH: /*Norte*/
					
						movery = mapa->info_naves[jefe_id][nave_id].posy - MOVER_ALCANCE;
						moverx = mapa->info_naves[jefe_id][nave_id].posx;
						if(0 <= mapa->info_naves[jefe_id][nave_id].posy - MOVER_ALCANCE){
							
			
							check = mapa_is_casilla_vacia(mapa, mapa->info_naves[jefe_id][nave_id].posy - MOVER_ALCANCE,mapa->info_naves[jefe_id][nave_id].posx);
							
						}
						break;
						case NORTH_EAST: /*Noreste*/
						
						movery = mapa->info_naves[jefe_id][nave_id].posy - MOVER_ALCANCE;
						moverx = mapa->info_naves[jefe_id][nave_id].posx + MOVER_ALCANCE;
						if((0 <= mapa->info_naves[jefe_id][nave_id].posy - MOVER_ALCANCE) && (MAPA_MAXX > mapa->info_naves[jefe_id][nave_id].posx + MOVER_ALCANCE)){					
							check = mapa_is_casilla_vacia(mapa, mapa->info_naves[jefe_id][nave_id].posy - MOVER_ALCANCE, mapa->info_naves[jefe_id][nave_id].posx + MOVER_ALCANCE);
							
						}
						break;
						case EAST: /*Este*/
						
						moverx = mapa->info_naves[jefe_id][nave_id].posx + MOVER_ALCANCE;
						movery = mapa->info_naves[jefe_id][nave_id].posy;
						if(MAPA_MAXX > mapa->info_naves[jefe_id][nave_id].posx + MOVER_ALCANCE){			
							check = mapa_is_casilla_vacia(mapa, mapa->info_naves[jefe_id][nave_id].posy,mapa->info_naves[jefe_id][nave_id].posx + MOVER_ALCANCE);
							
						}
						break;
						case SOUTH_EAST: /*Sureste*/
						
						movery = mapa->info_naves[jefe_id][nave_id].posy + MOVER_ALCANCE;
						moverx = mapa->info_naves[jefe_id][nave_id].posx + MOVER_ALCANCE;
						if((MAPA_MAXY > mapa->info_naves[jefe_id][nave_id].posy + MOVER_ALCANCE) && (MAPA_MAXX > mapa->info_naves[jefe_id][nave_id].posx + MOVER_ALCANCE)){					
							check = mapa_is_casilla_vacia(mapa, mapa->info_naves[jefe_id][nave_id].posy + MOVER_ALCANCE,mapa->info_naves[jefe_id][nave_id].posx + MOVER_ALCANCE);
							
						}
						break;
						case SOUTH: /*Sur*/
						
						movery = mapa->info_naves[jefe_id][nave_id].posy + MOVER_ALCANCE;
						moverx = mapa->info_naves[jefe_id][nave_id].posx;
						if(MAPA_MAXY > mapa->info_naves[jefe_id][nave_id].posy + MOVER_ALCANCE){			
							check = mapa_is_casilla_vacia(mapa, mapa->info_naves[jefe_id][nave_id].posy + MOVER_ALCANCE,mapa->info_naves[jefe_id][nave_id].posx);
							
						}
						break;
						case SOUTH_WEST: /*Suroeste*/

						movery = mapa->info_naves[jefe_id][nave_id].posy + MOVER_ALCANCE;
						moverx = mapa->info_naves[jefe_id][nave_id].posx - MOVER_ALCANCE;
						if((MAPA_MAXY > mapa->info_naves[jefe_id][nave_id].posy + MOVER_ALCANCE) && (0 <= mapa->info_naves[jefe_id][nave_id].posx - MOVER_ALCANCE)){					
							check = mapa_is_casilla_vacia(mapa, mapa->info_naves[jefe_id][nave_id].posy + MOVER_ALCANCE,mapa->info_naves[jefe_id][nave_id].posx - MOVER_ALCANCE);
							
						}
						break;
						case WEST: /*Oeste*/
						
						moverx = mapa->info_naves[jefe_id][nave_id].posx - MOVER_ALCANCE;
						movery = mapa->info_naves[jefe_id][nave_id].posy;
						if(0 <= mapa->info_naves[jefe_id][nave_id].posx - MOVER_ALCANCE){			
							check = mapa_is_casilla_vacia(mapa, mapa->info_naves[jefe_id][nave_id].posy, mapa->info_naves[jefe_id][nave_id].posx - MOVER_ALCANCE);
							
						}
						break;
						default: /*Noroeste*/
						
						movery = mapa->info_naves[jefe_id][nave_id].posy - MOVER_ALCANCE;
						moverx = mapa->info_naves[jefe_id][nave_id].posx - MOVER_ALCANCE;
						if((0 <= mapa->info_naves[jefe_id][nave_id].posy - MOVER_ALCANCE) && (0 <= mapa->info_naves[jefe_id][nave_id].posx - MOVER_ALCANCE)){					
							check = mapa_is_casilla_vacia(mapa, mapa->info_naves[jefe_id][nave_id].posy - MOVER_ALCANCE,mapa->info_naves[jefe_id][nave_id].posx - MOVER_ALCANCE);
							
						}
						break;
					}
					if(i == 8){
						break;
					}
				}
								
				if(check == true){
					/*Se escribe el mensaje*/
					message msg;
					msg.posy = mapa_get_nave(mapa,jefe_id,nave_id).posy;
					msg.posx = mapa_get_nave(mapa,jefe_id,nave_id).posx;
					msg.desty = movery;
					msg.destx = moverx;
					msg.numNave = nave_id;
					msg.equipo = jefe_id;
					strcpy(msg.tipo_accion,"MOVER");
					/*Se manda el mensaje*/
					
					if(mq_send(queue_msg, (char *)&msg, sizeof(msg), 1) == -1){
						fprintf (stderr, "Error sending message\n");
						return EXIT_FAILURE;
					}
				}
			}else if(strncmp(readbuffer,"DESTRUIR",8) == 0){
				close(fdd[nave_id][0]);
				return EXIT_SUCCESS;
			}
		}
	}




	int ret=0;

    exit(ret);
}
