/**
 * @brief Se intenta que un proceso escriba en la cadena y
 * otro la muestra en pantalla
 * @file p1_exercise5.c
 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
 * @version 1.0
 * @date 24-02-2019
 * @copyright GNU Public License
 */
#include <sys/types.h>
#include <sys/wait.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>

/** 
 * @brief Metodo que realiza la funcion principal
 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
 * @date 24-02-2019 
 * @return Devuelve exito o fracaso del metodo
 */
int  main ( void )
{
	pid_t pid;
	char * sentence = (char *)malloc(5 * sizeof (char));
	pid = fork();
	if (pid <  0  )
	{
		printf("Error al emplear fork\n");
		exit (EXIT_FAILURE);
	}
	else  if(pid ==  0)
	{	
		strcpy(sentence, "hola");
		free(sentence);
		exit(EXIT_SUCCESS);
	}
	else
	{
		wait(NULL);
		printf("Padre: %s\n", sentence);
		free(sentence);
		exit(EXIT_SUCCESS);
	}
}
