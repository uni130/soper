/**
 * @brief Se realiza un fork y se obtiene la iteracion del bucle
 * en la que se encuentra en ese momento
 * @file p1_exercise4b.c
 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
 * @version 1.0
 * @date 24-02-2019
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define NUM_PROC 3

/** 
 * @brief El padre crea hijos durante todas las iteraciones del bucle
 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
 * @date 24-02-2019 
 * @return Salida con exito o con fracaso
 */
int main(void)
{
	pid_t pid;
	int i;
	for(i = 0; i < NUM_PROC; i++)
	{
		pid = fork();
		if(pid <  0)
		{
			printf("Error al emplear fork\n");
			exit(EXIT_FAILURE);
		}
		else if(pid ==  0)
		{
			printf("HIJO: %d\n", i);
			exit(EXIT_SUCCESS);
		}
		else if(pid >  0)
		{
			printf("PADRE %d\n", i);
		}
	}

	for (i = 0; i < NUM_PROC; i++){
		wait(NULL);
	}
	exit(EXIT_SUCCESS);
}

