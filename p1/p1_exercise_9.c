/**
 * @brief El hijo obtiene un numero aleatorio, se lo pasa al 
 * padre y quien se lo pasa a otro hijo
 * @file p1_exercise9.c
 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
 * @version 1.0
 * @date 24-02-2019
 * @copyright GNU Public License
 */

#include <sys/types.h>
#include <sys/wait.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include<string.h>
#include<time.h>

/** 
 * @brief Genera un numero aleatorio
 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
 * @date 24-02-2019 
 * @return Devuelve el numero aleatorio
 */
int rando() {
  /* Inicializa el generador con una semilla cualquiera, este metodo solo
   se llama una vez */
  srand(time(NULL));
  /* Devuelve un numero aleatorio en 0 y MAX_RAND(un número alto que varia
   segun el sistema) */
  int r = rand();

  return r;
}

/** 
 * @brief Hace la funcionalidad principal
 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
 * @date 24-02-2019 
 * @return Salida con exito o con fracaso
 */
int main(void) {
  int fd[2];
  int fdd[2];
  int pipe_status,r;
  pid_t childpid,childpid2;

  int readbuffer;

  pipe_status=pipe(fd);
  pipe_status=pipe(fdd);
  if(pipe_status == -1) {
  	perror("Error creando la tuberia\n");
  	exit(EXIT_FAILURE);
  }

  if((childpid = fork()) == -1) {
 	perror("fork");
 	exit(EXIT_FAILURE);
  }

  if(childpid == 0) {
  	/* Cierre del descriptor de entrada en el hijo */
  	close(fd[0]);
  	/* Enviar el saludo vía descriptor de salida */
	r = rando();
	printf("hijo1 escribe el dato:%d\n", r);
  	write(fd[1], &r, sizeof(int));
	close(fd[1]);
  	exit(EXIT_SUCCESS);
  } else {
	childpid2 = fork();
	if(childpid2 == -1) {
 		perror("fork");
 		exit(EXIT_FAILURE);
  	}

  	else if(childpid2 == 0) {
  		close(fdd[1]);
  		read(fdd[0], &readbuffer, sizeof(int));
		printf("hijo2 recibe el dato :%d\n", readbuffer);
		close(fdd[0]);
  		exit(EXIT_SUCCESS);
  	} else {
  		/* Cierre del descriptor de salida en el padre */
  		close(fd[1]);
  		/* Leer el dato de la tuberia */
  		read(fd[0], &readbuffer, sizeof(int));
		r = readbuffer;
		close(fd[0]);
		close(fdd[0]);
		write(fdd[1], &r, sizeof(int));
		wait(NULL);
		wait(NULL);
		close(fdd[1]);
  		exit(EXIT_SUCCESS);
  	}
  }
}
