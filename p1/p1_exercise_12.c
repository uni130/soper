/**
 * @brief eleva a la potencia prefijada el 2
 * @file p1_exercise12.c
 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
 * @version 1.0
 * @date 24-02-2019
 * @copyright GNU Public License
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <math.h>
#define N 20

/*
* Estructura con el parametro constante del bucle
* y la variable que se eleva
*/
typedef struct{
	int x;
	int y;
}Data;

/** 
* @brief Guarda en el atributo "y" 2 elevado al numero guardado en el atributo "x"
* @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
* @date 24-02-2019 
*/
void *elevate(void* x){
	int res;
	Data* data = x;
	res = (int)pow(2,data->x);
	fflush(stdout);
	usleep(100000);
	data->y = res;
	pthread_exit(NULL);
}

/** 
* @brief Funcion principal, crea los hilos y muestra por pantalla
* el resultado
* @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
* @date 24-02-2019 
*/
int main(int argc , char *argv[]) {
   int i;
   pthread_t h[N];
   Data* data[N];
   for(i = 0;i < N;i++){
	data[i] = (Data*)malloc(sizeof(Data));
	if(!data[i]){
		for(i--;i >= 0;i--){
			free(data[i]);
		}
		return(EXIT_FAILURE);
	}
	data[i]->x = i;
	pthread_create(&h[i], NULL , elevate , data[i]);
   }
   for(i = 0;i < N;i++){
   	pthread_join(h[i],NULL);
	printf("2 elevado a %d es %d\n",data[i]->x,data[i]->y);
	free(data[i]);
   }
   
   printf("El programa %s termino correctamente \n", argv[0]);
   exit(EXIT_SUCCESS);
}
