#include <sys/types.h>
#include <sys/wait.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#define NUM_PROG 3
int main(void)
{
	pid_t pid;
	int i;
	for(i = 0; i < NUM_PROG; i++)
	{
		pid = fork();
		if(pid <  0)
		{
			printf("Error al emplear fork\n");
			exit(EXIT_FAILURE);
		}
		else if(pid ==  0)
		{
			printf("HIJO %d\n", i);
		}
		else if(pid >  0)
		{
			wait(NULL);
			printf("PADRE %d(hijo %d", i,i-1);
			if(i == 0)	printf("(Padre inicial)");
			printf(") eliminado\n");
			exit(EXIT_SUCCESS);
		}
	}
	wait(NULL);
	exit(EXIT_SUCCESS);
}


