/**
 * @brief Se realiza un fork y se obtienen los ids de los procesos
 * @file p1_exercise3.c
 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
 * @version 1.0
 * @date 24-02-2019
 * @copyright GNU Public License
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define NUM_PROC 3

/** 
 * @brief Divide el proceso y obtienes ids
 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
 * @date 24-02-2019 
 * @return Salida con exito o con fracaso
 */
int main(void)
{
	pid_t pid;
	int i;
	for(i = 0; i < NUM_PROC; i++)
	{
		pid = fork();
		if(pid <  0)
		{
			printf("Error al emplear fork\n");
			exit(EXIT_FAILURE);
		}
		else if(pid ==  0)
		{
			printf("PADRE: %d HIJO: %d\n", getppid(), getpid());
			exit(EXIT_SUCCESS);
		}
		else if(pid >  0)
		{
			printf("PADRE %d\n", i);
		}
	}
	wait(NULL);
	exit(EXIT_SUCCESS);
}

