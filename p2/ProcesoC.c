#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include <mqueue.h>

#define SIZE 2048

int main(int argc, char* argv[]){
	int fd;
	size_t size;
	const char* mapped;
	int i, status, check;
	struct stat s;
	mqd_t cola_mensajes;
	char mensaje[SIZE];
	long mensajesEnCola;


	struct mq_attr attributes = {
		.mq_flags = 0,
		.mq_maxmsg = 10,
		.mq_curmsgs = 0,
		.mq_msgsize = sizeof(SIZE)
	};

	cola_mensajes = mq_open(argv[1],O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, &attributes);
	if(cola_mensajes == (mqd_t)-1){
		fprintf (stderr, "Error opening the queue\n");
		return EXIT_FAILURE;
	}

	mq_getattr(cola_mensajes, &attributes);
	mensajesEnCola = attributes.mq_curmsgs;
	printf("La cantidad de mensajes en cola son: %ld\n", mensajesEnCola);

	for(i = 0; i < mensajesEnCola ; i++){
		check = mq_receive(cola_mensajes, (char *)&mensaje, SIZE, NULL);
		if(check == -1){
			perror("Error al recibir el mensaje\n");
		}
		printf("%s\n", mensaje);
	}

}
