  /**
   * @brief Programa que simula la lectura y escritura de procesos para 
   * aprender a aplicar las medidas necesarias para proteger una zona critica
   * @file p1_exercise3.c
   * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
   * @version 1.0
   * @date 15-03-2019
   * @copyright GNU Public License
   */

#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#define SEM "/example_sem"
#define N_READ 3
#define SECS 3
#define DOS 2

void Lectura(sem_t *semr, sem_t *semw, sem_t *lectores);
void Escritura(sem_t *semw, sem_t *semr);

/**
* @brief Funcion que imprime el semaforo pasado como argumento
* @param semaforo a imprimir
*/
void imprimir_semaforo(sem_t *sem){
  int sval;
  if(sem_getvalue(sem,&sval)==-1){
    perror("sem_getvalue");
    sem_unlink(SEM);
    exit(EXIT_FAILURE);
  }
    printf("Valor del semáforo: %d\n", sval);
    fflush(stdout);
}

/**
* @brief Funcion que instancia lo que hace el proceso al recibir la sennal
* definida por el manejador
* @param sennal que se va a recibir
*/
void manejador_SIGINT(int sig){
  int i;
  if(kill(getpid(),SIGTERM)<0){
    exit(EXIT_FAILURE);
  }
  for(i=0; i<N_READ; i++){
    wait(NULL);
  }
  exit(EXIT_SUCCESS);
}

/**
* @brief Funcion principal
* @retval int, sera 0 si la funcion termina con exito
*/
int main(){
    int i;
    int sval;
    sem_t *semr = NULL;
    sem_t *semw = NULL;
    sem_t *lectores = NULL;
    pid_t pid;
    struct sigaction act;
    act.sa_handler = manejador_SIGINT;
    sigemptyset(&(act.sa_mask));
    act.sa_flags = 0;
    if ((semr = sem_open("sem1", O_CREAT | O_EXCL, S_IRUSR , 1)) == SEM_FAILED) {
      perror("sem_open1");
      exit(EXIT_FAILURE);
    }
    if ((semw = sem_open("sem2", O_CREAT | O_EXCL, S_IWUSR, 1)) == SEM_FAILED) {
      perror("sem_open2");
      sem_unlink("sem1");
      exit(EXIT_FAILURE);
    }
    if ((lectores = sem_open("sem3", O_CREAT | O_EXCL, S_IWUSR, 0)) == SEM_FAILED) {
      perror("sem_open2");
      sem_unlink("sem1");
      sem_unlink("sem2");
      exit(EXIT_FAILURE);
    }
    sem_unlink("sem1");
    sem_unlink("sem2");
    sem_unlink("sem3");
    pid = fork();
    for(i = 1;i < N_READ;i++){
    	if (pid < 0) {
    	  perror("fork");
    	  exit(EXIT_FAILURE);
    	}
    	if (pid > 0) {
   			pid = fork();
   		}
	}
  if(pid<0){
    perror("fork");
    exit(EXIT_FAILURE);
  }
	if(!pid){
		while(1){
		Lectura(semr, semw, lectores);
		sleep(SECS);
		}

	}
	else if(pid){
    if (sigaction(SIGINT, &act, NULL) < 0) {
    printf("no he conseguido capturar la señal\n");
    perror("sigaction");
      exit(EXIT_FAILURE);
    }
    act.sa_handler = SIG_IGN;
    if (sigaction(SIGTERM, &act, NULL) < 0) {
    printf("no he conseguido capturar la señal\n");
    perror("sigaction");
      exit(EXIT_FAILURE);
    }
		while(1){
		Escritura(semw, semr);
		sleep(SECS);
		}
	}

	sem_close(semr);
	sem_close(semw);

	return 0;
}

/**
* @brief Funcion que immita la lectura
* @param semaforo de lectura
* @param semaforo de escritura
* @param semaforo que cuenta lectores
*/
void Lectura(sem_t *semr, sem_t *semw, sem_t *lectores) {
  int sval;
  sem_wait(semr);
  sem_post(lectores);
  sem_getvalue(lectores,&sval);
  if (sval == 1){
    sem_wait(semw);
  }
  sem_post(semr);
  printf("R-INI %ld\n", (long)getpid());
  sleep(3);
  printf("R-FIN %ld\n", (long)getpid());
  sem_wait(semr);
  sem_wait(lectores);
  sem_getvalue(lectores,&sval);
  if (sval == 0){
  	sem_post(semw);
  }
  sem_post(semr);
  return;
}

/**
* @brief Funcion que imita la escritura
* @param semaforo de escritura
* @param semaforo de lectura
*/
void Escritura(sem_t *semw, sem_t *semr) {
  sem_wait(semw);/*Escritura*/

  printf("W-INI %ld\n", (long)getpid());
  sleep(3);
  printf("W-FIN %ld\n", (long)getpid());
  sem_post(semw);/*Escritura*/
}
