	/**
	 * @brief Se hace una carrera de procesos y asi se comprueba la aleatoriiedad
	 * con la que se ejecutan los procesos cada vez que se ejecuta el programa
	 * mostrando asi la funcion del panificador
	 * @file p1_exercise3.c
	 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
	 * @version 1.0
	 * @date 15-03-2019
	 * @copyright GNU Public License
	 */

	#include <stdio.h>
	#include <stdlib.h>
	#include <unistd.h>
	#include <sys/types.h>
	#include <sys/wait.h>

	#define NUM_PROC 4

	/**
	* @brief manejador_SIGUSR2: Manda SIGUSR2 a su padre
	* @param sennal para la que se usa sigaction con el objetivo de
	* modificar la reaccion del proceso ante ella
	*/
	void manejador_SIGUSR2(int sig){
		return;
	}

	/**
	* @brief manejador_SIGUSR1: marca la primera llegada
	* @param sennal para la que se usa sigaction con el objetivo de
	* modificar la reaccion del proceso ante ella
	*/
	void manejador_SIGUSR1(int sig){
		return;
	}

	/**
	 * @brief Realiza una carrera entre los procesos hijos del proceso gestor
	 * @retval Salida con exito o con fracaso
	 */
	int main(void){
		pid_t pid, ppid;
		int i = 0;
	    struct sigaction act;
	    act.sa_handler = SIG_IGN;
	    sigemptyset(&(act.sa_mask));
	    act.sa_flags = 0;


		pid = fork();
		if(pid <  0){
			printf("Error al emplear fork primero\n");
			exit(EXIT_FAILURE);
		}

		/*Proceso gestor*/
		else if(pid ==  0){

			ppid = getppid();
			setpgid(0,ppid);

			/*Bucle de creacion de procesos hijos*/
			for(i = 0; i < NUM_PROC; i++){

				pid = fork();
				if (pid < 0){
					printf("Error al emplear fork segundo\n");
					exit(EXIT_FAILURE);
				}

				/*Procesos participantes de la carrera*/
				else if (pid == 0){
					printf("ppid : %d\n", (int)ppid);
					if(kill(getppid(),SIGUSR2)<0){
						exit(EXIT_FAILURE);
					}
					setpgid(0,ppid);

					/*Handle para saber que estan listos*/
					act.sa_handler = manejador_SIGUSR1;
	   			 	if (sigaction(SIGUSR1, &act, NULL) < 0) {
						printf("no he conseguido capturar la señal\n");
						perror("sigaction");
	    			 	exit(EXIT_FAILURE);
	  				}

					printf("Proceso hijo %d, listo\n", (int)getpid());

					/*Para hasta que esten todos los procesos listos, es decir,
					* cuando reciba SIGUSR1*/
					pause();

					printf("Primero el proceso: %d \n", (int)getpid());
					return(EXIT_SUCCESS);

				} else {

					act.sa_handler = manejador_SIGUSR2;
	   			 	if (sigaction(SIGUSR2, &act, NULL) < 0) {
						printf("no he conseguido capturar la señal\n");
						perror("sigaction");
	    			 	exit(EXIT_FAILURE);
	  				}

	  				/*Espera a que este listo el proceso antes de la siguinte
	  				* iteraccion del bucle*/
	  				pause();

				}


			}

	   		/*Notifica al padre de que tdos los procesos de a carrera estan listos*/
			kill(getppid() ,SIGUSR2);

			act.sa_handler = manejador_SIGUSR1;
			if (sigaction(SIGUSR1, &act, NULL) < 0) {
				printf("no he conseguido capturar la señal\n");
				perror("sigaction");
	    	 	exit(EXIT_FAILURE);
	  		}
			pause();

			/*Bucle que espera hasta que todos los procesos hijos hayan terminado*/
			for(i = 0; i < NUM_PROC; i++){
				wait(NULL);
			}
		}

		else if(pid >  0)
		{

			act.sa_handler = manejador_SIGUSR2;
	    	if (sigaction(SIGUSR2, &act, NULL) < 0) {
				printf("no he conseguido capturar la señal\n");
				perror("sigaction");
	     		exit(EXIT_FAILURE);
	   		}

	   		/*Espera hasta que recibir del proceso gestor que los procesos de la carrera
	   		* estan listos*/
	   		pause();

			act.sa_handler = manejador_SIGUSR1;
			if (sigaction(SIGUSR1, &act, NULL) < 0) {
				printf("no he conseguido capturar la señal\n");
				perror("sigaction");
	     		exit(EXIT_FAILURE);
	   		}

			killpg(getpid(), SIGUSR1);

			wait(NULL);

			return(EXIT_SUCCESS);
		}

	}
