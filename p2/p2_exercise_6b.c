	/**
	 * @brief Intenta capturar sigterm
	 * @file p1_exercise3.c
	 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
	 * @version 1.0
	 * @date 15-03-2019
	 * @copyright GNU Public License
	 */

	#include <stdio.h>
	#include <stdlib.h>
	#include <signal.h>
	#include <sys/wait.h>
	#include <unistd.h>
	#include <time.h>

	#define N_ITER 5
	#define SECS 40

	void manejador_SIGALRM(int sig){
		return;
	}

	int main (void) {
	 pid_t pid;
	 int counter;
	 struct sigaction act;

	 sigemptyset(&(act.sa_mask));
	 act.sa_flags =0;
	 act.sa_handler = manejador_SIGALRM;

	 if (sigaction(SIGALRM, &act, NULL) < 0) {
						printf("no he conseguido capturar la señal\n");
						perror("sigaction");
	    			 	exit(EXIT_FAILURE);
	 }

	 pid = fork();
	 if (pid < 0) {
	   perror("fork");
	   exit(EXIT_FAILURE);
	 }
	 if (pid > 0) {

	   if(alarm(SECS)){
	      fprintf(stderr, "No se pudo iniciar la alarma\n");
	   }

	     pause();
	     kill(pid,SIGTERM);
	     wait(NULL);
	     printf("he matado al hijo, voy a acabar.");
	 }
	}
