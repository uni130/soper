	/**
	 * @brief Se realiza un fork y se obtienen los ids de los procesos
	 * @file p1_exercise3.c
	 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
	 * @version 1.0
	 * @date 24-02-2019
	 * @copyright GNU Public License
	 */

	#include <stdio.h>
	#include <stdlib.h>
	#include <unistd.h>
	#include <sys/types.h>
	#include <sys/wait.h>

	#define NUM_PROC 4

	/**
	 * @brief Divide el proceso y obtienes ids
	 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
	 * @date 24-02-2019
	 * @retval Salida con exito o con fracaso
	 */
	int main(void)
	{
		pid_t pid;
		int i;
		for(i = 0; i < NUM_PROC; i++)
		{
			pid = fork();
			if(pid <  0)
			{
				printf("Error al emplear fork\n");
				exit(EXIT_FAILURE);
			}
			else if(pid ==  0)
			{
				printf("Soy el proceso hijo:%d\n", (int)getpid());
				sleep(30);
				printf("Soy el proceso hijo:%d y ya me toca terminar\n", (int)getpid());
				exit(EXIT_SUCCESS);
			}
			else if(pid >  0)
			{
				sleep(5);
				if(kill(pid,SIGTERM)<0){
					exit(EXIT_FAILURE);
				}
			}
		}
		for(i = 0; i < NUM_PROC; i++){
			wait(NULL);
		}
		exit(EXIT_SUCCESS);
	}
