/**
 * @brief Se hace un proceso que lee un fichero, lo divide en fragmentos de 2KB y a continuacion
 * va pasando estos fragmentos como mensajes de la cola de mensajes
 * @file procesoA.c
 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
 * @version 1.0
 * @date 10-04-2019
 * @copyright GNU Public License
 */

#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include <mqueue.h>

#define MQ_NAME "/mq_ejercicio4"
#define SIZE 2048
/*Funcion principal*/
int main(int argc,char* argv[]){
	int fd, longitud, cociente;
	size_t size;
	const char* mapped;
	int i, status, j;
	struct stat st;
	mqd_t cola_mensajes;
	char *mensaje, msg[SIZE];

	struct mq_attr attributes = {
		.mq_flags = 0,
		.mq_maxmsg = 10,
		.mq_curmsgs = 0,
		.mq_msgsize = SIZE + 1
	};
	struct mq_attr attributes2;

	/*Abre el fichero*/
	fd = open(argv[1], O_RDONLY);
	if(fd == -1){
		return EXIT_FAILURE;
	}
	/*Tamaño del fichero?*/
	fstat(fd, &st);
  	printf("Size: %lu\n", (u_int64_t)st.st_size);
	size = st.st_size;
	/*Mapea el fichero*/
	mapped = (char*)mmap(0, size, PROT_READ, MAP_SHARED, fd, 0);
	if(mapped == NULL){
		return EXIT_FAILURE;
	}

	/*Obtiene la llongitud del fichero y la divide para saber el numero de fragmentos*/
	longitud = strlen(mapped);
	cociente = longitud/SIZE;
	/*Estas condiciones sirven para comprobar casos excepcionales*/
	if(!longitud%SIZE) cociente--;
	if(cociente > 8) cociente = 8;

	cola_mensajes = mq_open(argv[2], O_RDWR, S_IRUSR | S_IWUSR, &attributes);
	if(cola_mensajes == (mqd_t)-1){
		fprintf (stderr, "Error opening the queue\n");
		return EXIT_FAILURE;
	}

	for(i = 0; i <= cociente; i++){
		if(mq_send(cola_mensajes, &mapped[i*SIZE], SIZE, 1) == -1)	{
				perror("Error sending message\n");
				return EXIT_FAILURE;
			}
	}
	return 0;


}
