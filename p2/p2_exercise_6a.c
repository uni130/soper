    /**
   * @brief Se hace una mascara para que no pueda recibir la alarma
   * despues se cambia la mascara y el proceso se para porque recibe la alarma
   * @file p1_exercise3.c
   * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
   * @version 1.0
   * @date 20-03-2019
   * @copyright GNU Public License
   */

  #include <stdio.h>
  #include <stdlib.h>
  #include <signal.h>
  #include <sys/wait.h>
  #include <unistd.h>
  #include <time.h>

  #define N_ITER 5
  #define SECS 40

  /**
  * @brief funcion principal del programa
  * @retval devuelve 0 si el programa termina con exito
  */
  int main (void) {
   pid_t pid;
   int counter;
   sigset_t bloq, desbloq;
   struct sigaction act;
   act.sa_handler = SIG_DFL;
   sigemptyset(&(act.sa_mask));
   act.sa_flags = 0;


   pid = fork();
   if (pid < 0) {
     perror("fork");
     exit(EXIT_FAILURE);
   }
   if (pid == 0) {

     if(alarm(SECS)){
        fprintf(stderr, "No se pudo iniciar la alarma\n");
     }

     sigemptyset(&(bloq));
     sigaddset(&(bloq), SIGUSR1);
     sigaddset(&(bloq), SIGUSR2);
     sigaddset(&(bloq), SIGALRM);

     sigprocmask(SIG_BLOCK, &(bloq), NULL);

     while(1) {
       for (counter = 0; counter < N_ITER; counter++) {
         printf("%d\n", counter);
         sleep(1);
         }
       sigdelset(&(bloq),SIGALRM);
       sigaddset(&(desbloq), SIGALRM);
       sigaddset(&(desbloq), SIGUSR1);
       sigprocmask(SIG_UNBLOCK, &(desbloq), NULL);
       if (sigaction(SIGALRM, &act, NULL) < 0) {
          printf("no he conseguido capturar la señal\n");
          perror("sigaction");
          exit(EXIT_FAILURE);
    }
       sleep(6);
       }



     }
     while (wait(NULL) > 0);
  }
