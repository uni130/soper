#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include <mqueue.h>

#define MQ_NAME "/mq_ejercicio4"
#define SIZE 2048
int main(int argc,char* argv[]){
  struct mq_attr attributes = {
    .mq_flags = 0,
    .mq_maxmsg = 10,
    .mq_curmsgs = 0,
    .mq_msgsize = SIZE + 1
  };
  struct mq_attr attributes2;

  mqd_t cola_mensajes;
  mqd_t cola_mensajes2;
  int i, check, j;
  long  mensajesEnCola;
  char msg[SIZE + 1];
  cola_mensajes = mq_open(argv[1],O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, &attributes);
  if(cola_mensajes == (mqd_t)-1){
    fprintf (stderr, "Error opening the queue\n");
    return EXIT_FAILURE;
  }
  cola_mensajes2 = mq_open(argv[2], O_CREAT | O_RDWR |  O_NONBLOCK, S_IRUSR | S_IWUSR ,&attributes);
  if(cola_mensajes2 == (mqd_t)-1){
    fprintf (stderr, "Error opening the queue\n");
    return EXIT_FAILURE;
  }
  mq_getattr(cola_mensajes, &attributes2);
  mensajesEnCola = attributes2.mq_curmsgs;
  printf("La cantidad de mensajes en cola son: %ld\n", mensajesEnCola);

  for(j = 0; j < mensajesEnCola ; j++){
    check = mq_receive(cola_mensajes, (char *)&msg, SIZE + 1, NULL);
    if(check == -1){
      perror("Error al recibir el mensaje\n");
    }
    printf("(%s)\n", msg);
    for(i = 0;i < SIZE;i++){
      if(msg[i] <= 'z' && msg[i] >= 'a'){
        msg[i] = 'z' - msg[i] + 'a';
      }else if(msg[i] <= 'Z' && msg[i] >= 'A'){
        msg[i] = 'Z' - msg[i] + 'A';
      }
      }
      printf("(%s)\n", msg);
      if(mq_send(cola_mensajes2, msg, SIZE, 1) == -1)	{
          perror("Error sending message\n");
          return EXIT_FAILURE;
    }
  }
  mq_getattr(cola_mensajes2, &attributes2);
  mensajesEnCola = attributes2.mq_curmsgs;
  printf("La cantidad de mensajes en cola son: %ld\n", mensajesEnCola);

return 0;
}
