	/**
	 * @brief Se hace una carrera de procesos y asi se comprueba la aleatoriiedad
	 * con la que se ejecutan los procesos cada vez que se ejecuta el programa
	 * mostrando asi la funcion del panificador
	 * @file p1_exercise3.c
	 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
	 * @version 1.0
	 * @date 15-03-2019
	 * @copyright GNU Public License
	 */

	#include <stdio.h>
	#include <stdlib.h>
	#include <semaphore.h>
	#include <fcntl.h>
	#include <sys/stat.h>
	#include <sys/wait.h>
	#include <sys/types.h>
	#include <unistd.h>
	#include <time.h>

	#define N_PROC 3

	int Lectura(sem_t *semw,char* path);
	void Escritura(sem_t *semw,char* path,int id);
	int aleat_num(int inf, int sup);

	/**
	* @brief manejador_SIGUSR1: termina el proceso
	* @param senial para la que se usa sigaction
	*/
	void manejador_SIGUSR1(int sig){
		exit(EXIT_SUCCESS);
		return;
	}
	/**
	 * @brief Realiza una carrera entre los procesos hijos del proceso gestor
	 * con el objetivo de ver quien escribe mas rapido
	 */
void main(){
    int i;
    int id = -1;
    int sval;
    sem_t *semw = NULL;
    pid_t pid;
    struct sigaction act;
    act.sa_handler = manejador_SIGUSR1;
    sigemptyset(&(act.sa_mask));
    act.sa_flags = 0;
    srand(time(NULL));
    if ((semw = sem_open("sem2", O_CREAT | O_EXCL, S_IWUSR, 1)) == SEM_FAILED) {
        perror("sem_open2");
        exit(EXIT_FAILURE);
    }
    sem_unlink("sem2");
    pid = fork();
    for(i = 1;i < N_PROC;i++){
        if (pid < 0) {
            perror("fork");
            exit(EXIT_FAILURE);
        }
        if ((pid == 0)&&(id==-1)) {
            id = i-1;
        }
        if (pid > 0) {
            pid = fork();
        }
    }
    if ((pid == 0)&&(id==-1)) {
        id = i-1;
    }
    if(pid<0){
        perror("fork");
        exit(EXIT_FAILURE);
    }
    if(!pid){
        if (sigaction(SIGUSR1, &act, NULL) < 0) {
            printf("no he conseguido capturar la señal\n");
            perror("sigaction");
            exit(EXIT_FAILURE);
        } 
        while(1){
            Escritura(semw,"texto.txt",id);
            usleep(aleat_num(0, 100000));
        }
    }
    else if(pid){
        act.sa_handler = SIG_IGN;
        if (sigaction(SIGUSR1, &act, NULL) < 0) {
            printf("no he conseguido capturar la señal\n");
            perror("sigaction");
            exit(EXIT_FAILURE);
        }
    while(id < 0){
        id = Lectura(semw,"texto.txt");
        sleep(1);
    }
    if(killpg(getpid(),SIGUSR1)<0){
        exit(EXIT_FAILURE);
    }
    for(i = 0;i < N_PROC;i++){
        wait(NULL);
    }
    printf("el ganador es: %d\n",id);
    }

    sem_close(semw);

    return;
}

/**
* @brief aleat_num: devuelve un numero aleatorio entre los valores introducidos
* @param inf valor mas pequeño que puede devolver
* @param sup valor mas grande que puede devolver
*/
int aleat_num(int inf, int sup) {
    if (sup < inf) {
        return -1;
    }

    return (int) (inf + (double) rand() / (RAND_MAX + 1.0)*(sup - inf + 1));

}

/**
* @brief Lectura: lee el fichero y cuenta cuantas veces ha escrito cada proceso
* @param semw semaforo para controlar el acceso al fichero
* @param path ruta del fichero
*/
int Lectura( sem_t *semw,char* path) {
    FILE* f;
    int val,i;
    char chain[5];
    int ganador = -1;
    int repeticiones[N_PROC];
    sem_wait(semw);
    for(i = 0;i < N_PROC;i++){
        repeticiones[i] = 0;
    }
    f = fopen(path,"r");
    while(!feof(f)){
        fgets(chain,sizeof(int),f);
        val = atoi(chain);
        (repeticiones[val])++;
    }
    fclose(f);
    for(i = 0;i < N_PROC;i++){
        printf("el proceso %d ha escrito %d veces\n",i,repeticiones[i]);
        if(repeticiones[i] >= 20){
            if(ganador < 0){
                ganador = i; 
            }else if(repeticiones[i]>repeticiones[ganador]){
                ganador = i;
            }
        }
    }
    f = fopen(path,"w");
    fclose(f); 
    sem_post(semw);
    return ganador;
}

/**
* @brief Escritura: escribe su id en el fichero
* @param semw semaforo para controlar el acceso al fichero
* @param path ruta del fichero
* @param id identificador del proceso entrante
*/
void Escritura(sem_t *semw,char* path,int id) {
    FILE* f;
    sem_wait(semw);
    f = fopen(path,"a+");
    fprintf(f,"%d\n",id);
    fclose(f);
    sem_post(semw);
}
