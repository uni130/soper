/**
 * @brief Se hace un proceso principal por el cual se llaman a todos los demás que realizan
 * las funciones necesarias
 * @file programa_principal.c
 * @author Miguel Angel Luque Lopez & Magdalena Herrera Soto
 * @version 1.0
 * @date 10-04-2019
 * @copyright GNU Public License
 */

#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include <mqueue.h>
#define SIZE 2048

/**
 * @brief Funcion principal del programa que ejecuta los tres otros programas
 */
int main(int argc, char* argv[]){
	int fd;
	size_t size;
	const char* mapped;
	int i, status, j;
	struct stat s;
	mqd_t cola_mensajes1, cola_mensajes2;
	char *mensaje;
	pid_t pid;

	struct mq_attr attributes = {
		.mq_flags = 0,
		.mq_maxmsg = 10,
		.mq_curmsgs = 0,
		.mq_msgsize = SIZE
	};

/*Se abren las colas de mensajes*/
	cola_mensajes1 = mq_open(argv[2],O_CREAT | O_RDWR,
S_IRUSR | S_IWUSR, &attributes);
	if(cola_mensajes1 == (mqd_t)-1){
		fprintf (stderr, "Error abriendo la cola_mensajes1\n");
		return EXIT_FAILURE;
	}

	cola_mensajes2 = mq_open(argv[3],O_CREAT | O_RDWR,
S_IRUSR | S_IWUSR, &attributes);
	if(cola_mensajes2 == (mqd_t)-1){
		fprintf (stderr, "Error abriendo la cola_mensajes2\n");
		return EXIT_FAILURE;
	}

	/*Se crean los procesos hijo*/
	pid = fork();
	/*Se ejecuta el programa que lee el fichero y manda le primer mensaje*/
	if( pid == 0){
		execl("ProcesoA","ProcesoA", argv[1], argv[2], NULL);
	} else if (pid < 0) {
		mq_close(cola_mensajes1);
		mq_unlink(argv[2]);
		mq_close(cola_mensajes2);
		mq_unlink(argv[3]);
	}
	wait(NULL);
	pid = fork();
	/*Se ejecuta el programa que lee el mensaje y lo transforma para luego mandarlo de nuevo*/
	if( pid == 0){
		execl("ProcesoB","ProcesoB", argv[2], argv[3], NULL);
	} else if (pid < 0) {
		mq_close(cola_mensajes1);
		mq_unlink(argv[2]);
		mq_close(cola_mensajes2);
		mq_unlink(argv[3]);
	}
	wait(NULL);
	pid = fork();
	/*Lee el mensaje de la segunda cola y lo muestra por pantalla*/
	if( pid == 0){
		execl("ProcesoC","ProcesoC", argv[3], NULL);
	} else if (pid < 0) {
		mq_close(cola_mensajes1);
		mq_unlink(argv[2]);
		mq_close(cola_mensajes2);
		mq_unlink(argv[3]);
	}
	wait(NULL);
/*Cierra todos los recursos utilizados*/
	mq_close(cola_mensajes1);
	mq_unlink(argv[2]);
	mq_close(cola_mensajes2);
	mq_unlink(argv[3]);

}
